import React, {useState, useEffect} from "react";
import axios from "axios";
import './App.css';
import CountriesList from "../../components/CountriesList/CountriesList";
import CountriesData from "../../components/CountriesData/CountriesData";
import withLoadingHandler from "../../hoc/withLoadingHandler/withLoadingHandler";

const BASE_URL = 'https://restcountries.eu/rest/v2/'

const App = () => {

    const [countries, setCountries] = useState([]);
    const [countryInfo, setCountryInfo] = useState([]);
    const [borders, setBorders] = useState([])

    useEffect(() => {
        countryList();
    }, [])

    const countryList = () => {
        try {
            axios.get(BASE_URL + 'all?fields=name;alpha3Code').then(result => {
                setCountries(result.data)
            })
        } catch (e) {
            console.log(e);
        }
    };

    const onClickHandler = async (code) => {
        try {
            const response = await axios.get(BASE_URL + `alpha/${code}`);
            let borders = [];
            for (let item of response.data.borders) {
                const result = await axios.get(BASE_URL +`alpha/${item}` );
                borders.push(result.data);
            }
            setCountryInfo(response.data);
            setBorders(borders)
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <div className="App">
            <div className={'flex'}>
                <ul className="list">
                    {countries.map((country, index) => (
                        <CountriesList
                            key={index}
                            name={country.name}
                            onClickHandler={() => onClickHandler(country['alpha3Code'])}
                        />
                    ))}
                </ul>
                <CountriesData
                    borders={borders}
                    countryInfo={countryInfo}
                />

            </div>
        </div>
    )
};

export default withLoadingHandler(App, axios);
