import React, {useState, useEffect} from 'react';
import Modal from "../../components/UI/Modal/Modal";
import Spinner from "../../components/UI/Spinner/Spinner";

const withLoadingHandler = (WrappedComponent, axios) => {
    return function WithLoadingHandler(props) {
        const [loading, setLoading] = useState(false);

        useEffect(() => {
            axios.interceptors.request.use(res => {
                setLoading(true)
                return res;
            });
        }, []);

        useEffect(() => {
            axios.interceptors.response.use(res => {
                setLoading(false)
                return res;
            }, error => {
                setLoading(false);
                throw loading
            });
        }, [loading]);


        return (
            <>
                <Modal show={loading}>
                    <Spinner/>
                </Modal>
                <WrappedComponent {...props}/>
            </>
        );
    };
};

export default withLoadingHandler;